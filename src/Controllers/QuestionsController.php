<?php


namespace Ucc\Controllers;


use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController
{
    private $questionService;

    public function __construct(QuestionService $questionService)
    {
        // parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame()
    {

        $name = isset($_POST["name"]) && trim($_POST["name"]) != "" ? $_POST["name"] : null;
        if (empty($name)) {
            return $this->json(["message" => 'You must provide a name'], 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 0);
        Session::set('points', 0);


        //TODO Get first question for user
        $questions = $this->questionService->getRandomQuestions();
        Session::set('questions', json_encode($questions));
        

        // set question without answer and points
        $question["question"] = $questions[Session::get('questionCount')]->question;
        $question["possibleAnswers"] = $questions[Session::get('questionCount')]->possibleAnswers;

        return $this->json(['question' => $question], 200);
    }

    public function answerQuestion() {
        if ( Session::get('name') === null ) {
            return $this->json(["message" => 'You must first begin a game'], 400);
        }

        if ((int)Session::get('questionCount') >= 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"], 200);
        }

        $answer = isset($_POST["answer"]) && trim($_POST["answer"]) != "" ? $_POST["answer"] : null;
        if (empty($answer)) {
            return $this->json(["message" => 'You must provide an answer'], 400);
        }

        //TODO: Check answer and increment user's points. Reply with a proper message
        $questions = (Array)json_decode(Session::get('questions'));
        $questionsCount = Session::get('questionCount');
        $points = Session::get('points');

        

        $pointsForAnswer = $this->questionService->getPointsForAnswer($questionsCount, $answer, $questions);
        
        $message = ($questionsCount+1).' out of 5 questions answered. '.(4-$questionsCount).' more to go.';

        // update sesssion variables
        Session::set('questionCount', $questionsCount+1);
        Session::set('points', $points + $pointsForAnswer);

        // get next question
        $question["question"] = $questions[Session::get('questionCount')]->question;
        $question["possibleAnswers"] = $questions[Session::get('questionCount')]->possibleAnswers;

        return $this->json(['message' => $message, 'question' => $question], 200);
    }

    public function json($body, $code){
        $response = array(
            "code" => $code,
            "data" => $body
        );

        echo json_encode($response);
    }
}