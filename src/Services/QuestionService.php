<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private $json;
    private $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
    }

    public function getRandomQuestions(int $count = 5): array
    {
        //TODO: Get {$count} random questions from JSON
        $json = file_get_contents("questions.json");
        $questionsJson = array('json' => json_decode($json));
        $questions = $questionsJson["json"];
        shuffle($questions);

        return array_slice($questions, 0, 5);   ;
    }

    public function getPointsForAnswer(int $questionsCount, string $answer, array $questions): int
    {
        //TODO: Calculate points for the answer given
        if ($questions[$questionsCount]->correctAnswer == $answer) {
            return $questions[$questionsCount]->points;
        }

        return 0;
    }
}